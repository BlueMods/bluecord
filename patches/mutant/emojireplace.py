#!/bin/env python3
import os
import re
import shutil

# You'll likely want to edit these lines or set DISTOK_EXTRACTED_DISCORD_PATH and DISTOK_EMOJI_MUTANT
extracted_discord_path = os.environ.get("DISTOK_EXTRACTED_DISCORD_PATH", "/tmp/cutthecord/discord")
extracted_mutstd_path = os.environ.get("DISTOK_EMOJI_MUTANT", "/root/distokfiles/mutant/72x72")


# Add your custom emojis here
# with "mutstd filename": "discord filename".
# You'll need to write a patch for `assets/data/emojis.json` too.
custom_emojis = {"1f4af-200d-1f308.png": "emoji_1f4af_1f308.webp",
                 "10169b-200d-1f308.png": "emoji_10169b_1f308.webp",
                 "1f9d1-200d-2708-fe0f.png": "emoji_1f9d1_2708.webp",
                 "1f9d1-200d-2695-fe0f.png": "emoji_1f9d1_2695.webp",
                 "1f9d1-200d-1f680.png": "emoji_1f9d1_1f680.webp",
                 "1f9d1-200d-1f52c.png": "emoji_1f9d1_1f52c.webp",
                 "1f9d1-200d-1f527.png": "emoji_1f9d1_1f527.webp",
                 "1f9d1-200d-1f4bb.png": "emoji_1f9d1_1f4bb.webp",
                 "1f9d1-200d-1f373.png": "emoji_1f9d1_1f373.webp",
                 "1f3fb.png": "emoji_1f3fb.webp",
                 "1f3fc.png": "emoji_1f3fc.webp",
                 "1f3fd.png": "emoji_1f3fd.webp",
                 "1f3fe.png": "emoji_1f3fe.webp",
                 "1f3ff.png": "emoji_1f3ff.webp",
                 "101600.png": "emoji_101600.webp",
                 "101601.png": "emoji_101601.webp",
                 "101602.png": "emoji_101602.webp",
                 "101603.png": "emoji_101603.webp",
                 "101604.png": "emoji_101604.webp",
                 "101605.png": "emoji_101605.webp",
                 "101606.png": "emoji_101606.webp",
                 "101607.png": "emoji_101607.webp",
                 "101608.png": "emoji_101608.webp",
                 "101609.png": "emoji_101609.webp",
                 "10160a.png": "emoji_10160a.webp",
                 "10160b.png": "emoji_10160b.webp",
                 "10160c.png": "emoji_10160c.webp",
                 "10160d.png": "emoji_10160d.webp",
                 "10160e.png": "emoji_10160e.webp",
                 "10160f.png": "emoji_10160f.webp",
                 "101610.png": "emoji_101610.webp",
                 "101611.png": "emoji_101611.webp",
                 "101612.png": "emoji_101612.webp",
                 "101613.png": "emoji_101613.webp",
                 "101614.png": "emoji_101614.webp",
                 "101615.png": "emoji_101615.webp",
                 "101616.png": "emoji_101616.webp",
                 "101617.png": "emoji_101617.webp",
                 "101618.png": "emoji_101618.webp",
                 "101619.png": "emoji_101619.webp",
                 "10161a.png": "emoji_10161a.webp",
                 "10161b.png": "emoji_10161b.webp",
                 "10161c.png": "emoji_10161c.webp",
                 "10161d.png": "emoji_10161d.webp",
                 "10161e.png": "emoji_10161e.webp",
                 "10161f.png": "emoji_10161f.webp",
                 "101620.png": "emoji_101620.webp",
                 "101621.png": "emoji_101621.webp",
                 "101622.png": "emoji_101622.webp",
                 "101623.png": "emoji_101623.webp",
                 "101624.png": "emoji_101624.webp",
                 "101625.png": "emoji_101625.webp",
                 "101626.png": "emoji_101626.webp",
                 "101627.png": "emoji_101627.webp",
                 "101628.png": "emoji_101628.webp",
                 "101629.png": "emoji_101629.webp",
                 "10162a.png": "emoji_10162a.webp",
                 "10162b.png": "emoji_10162b.webp",
                 "10162c.png": "emoji_10162c.webp",
                 "101650.png": "emoji_101650.webp",
                 "101651.png": "emoji_101651.webp",
                 "10169a.png": "emoji_10169a.webp",
                 "26b2-fe0f.png": "emoji_26b2.webp",
                 "26a8-fe0f.png": "emoji_26a8.webp",
                 "26a7-fe0f.png": "emoji_26a7.webp",
                 "26a5-fe0f.png": "emoji_26a5.webp",
                 "26a4-fe0f.png": "emoji_26a4.webp",
                 "26a3-fe0f.png": "emoji_26a3.webp",
                 "26a2-fe0f.png": "emoji_26a2.webp",
                 "2642-fe0f.png": "emoji_2642.webp",
                 "2640-fe0f.png": "emoji_2640.webp",
                 "1f9e1.png": "emoji_1f9e1.webp",
                 "101685.png": "emoji_101685.webp",
                 "101684.png": "emoji_101684.webp",
                 "101683.png": "emoji_101683.webp",
                 "101682.png": "emoji_101682.webp",
                 "1f9fb.png": "emoji_1f9fb.webp",
                 "1f9f1.png": "emoji_1f9f1.webp",
                 "1f9e8.png": "emoji_1f9e8.webp",
                 "1f9b4.png": "emoji_1f9b4.webp",
                 "101696.png": "emoji_101696.webp",
                 "101695.png": "emoji_101695.webp",
                 "101694.png": "emoji_101694.webp",
                 "101693.png": "emoji_101693.webp",
                 "101692.png": "emoji_101692.webp",
                 "10169b.png": "emoji_10169b.webp",
                 "101698.png": "emoji_101698.webp",
                 "101699.png": "emoji_101699.webp",
                 "1f9dd.png": "emoji_1f9dd.webp",
                 "1f99d.png": "emoji_1f99d.webp",
                 "1f99c.png": "emoji_1f99c.webp",
                 "1f99a.png": "emoji_1f99a.webp",
                 "101666.png": "emoji_101666.webp",
                 "1f9d0.png": "emoji_1f9d0.webp",
                 "1f97a.png": "emoji_1f97a.webp",
                 "1f976.png": "emoji_1f976.webp",
                 "1f975.png": "emoji_1f975.webp",
                 "1f974.png": "emoji_1f974.webp",
                 "1f973.png": "emoji_1f973.webp",
                 "1f970.png": "emoji_1f970.webp",
                 "1f92f.png": "emoji_1f92f.webp",
                 "1f92e.png": "emoji_1f92e.webp",
                 "1f92d.png": "emoji_1f92d.webp",
                 "1f92c.png": "emoji_1f92c.webp",
                 "1f92b.png": "emoji_1f92b.webp",
                 "1f92a.png": "emoji_1f92a.webp",
                 "1f929.png": "emoji_1f929.webp",
                 "1f928.png": "emoji_1f928.webp",
                 "1f575-10162b.png": "emoji_1f575_10162b.webp",
                 "1f486-10162b.png": "emoji_1f486_10162b.webp",
                 "1f481-10162b.png": "emoji_1f481_10162b.webp",
                 "101690.png": "emoji_101690.webp",
                 "101697.png": "emoji_101697.webp",
                 "1f9dc.png": "emoji_1f9dc.webp",
                 "101681.png": "emoji_101681.webp",
                 "101680.png": "emoji_101680.webp",
                 "10167f.png": "emoji_10167f.webp",
                 "10167e.png": "emoji_10167e.webp",
                 "10167d.png": "emoji_10167d.webp",
                 "10167c.png": "emoji_10167c.webp",
                 "10167b.png": "emoji_10167b.webp",
                 "10167a.png": "emoji_10167a.webp",
                 "101679.png": "emoji_101679.webp",
                 "101678.png": "emoji_101678.webp",
                 "101677.png": "emoji_101677.webp",
                 "101676.png": "emoji_101676.webp",
                 "101675.png": "emoji_101675.webp",
                 "101674.png": "emoji_101674.webp",
                 "101673.png": "emoji_101673.webp",
                 "101672.png": "emoji_101672.webp",
                 "101671.png": "emoji_101671.webp",
                 "101670.png": "emoji_101670.webp",
                 "101686.png": "emoji_101686.webp",
                 "101691.png": "emoji_101691.webp"}


# self note to get MM emojos:
# ls 72x72 | grep "101650-" | cut -f 1 -d '-' | sort | uniq
# ls 72x72 | grep "101651-" | cut -f 1 -d '-' | sort | uniq
# echo -e "\U1f44c"
# paw: 􁙐
# claw: 􁙑

def add_diverse_emojos():
    # Hackiest regex ever
    div_regex = r'(|.*/)([a-f0-9]+(|-fe0f)-(|101650|101651|101650-|101651-)'\
                r'(|1016[0-2][a-f0-9]|1f3f[b-f])\.png)'
    div_regex = re.compile(div_regex)
    for file in mutstd_emojis:
        re_result = div_regex.fullmatch(file)
        if re_result:
            mutant = re_result.group(2)
            discord = discordify_emoji_name(mutant)
            custom_emojis[mutant] = discord


def clean_emoji_name(name):
    name = name.lower().replace("_", "-")\
        .replace("emoji-", "").replace("-fe0f", "")
    return name


def discordify_emoji_name(name):
    name = "emoji_" + name.lower().replace("-", "_").replace("fe0f_", "")\
        .replace("200d_", "").replace(".png", ".webp")
    return name


discord_emoji_path = os.path.join(extracted_discord_path, "res", "raw")
# Get file listings in relevant folders
discord_emojis = os.listdir(discord_emoji_path)
mutstd_emojis = os.listdir(extracted_mutstd_path)

add_diverse_emojos()

# Clean names of mutantstd emojis so thar we can compare them
# to clean discord emojis later
clean_mutstd_emojis = {clean_emoji_name(emoji): emoji for
                       emoji in mutstd_emojis}

replace_counter = 0

# Go through each discord emoji, and clean their names
for emoji in discord_emojis:
    clean_discord_emoji = clean_emoji_name("{}.png".format(os.path.splitext(emoji)[0]))

    # Check if said clean name of emoji is in clean mutstd list
    if clean_discord_emoji in clean_mutstd_emojis:
        # Get full unclean filename of mutantstd emoji, generate relevant paths
        full_mutstd_name = clean_mutstd_emojis[clean_discord_emoji]
        full_mutstd_path = os.path.join(extracted_mutstd_path, full_mutstd_name)
        full_discord_path = os.path.join(discord_emoji_path, emoji)

        # Copy and overwrite the discord emojis with the mutantstd alternatives
        shutil.copyfile(full_mutstd_path, full_discord_path)

        # print("Replaced {} emoji.".format(emoji))
        replace_counter += 1

for custom_emoji in custom_emojis:
    # One day I'd like to do discordify_emoji_name(custom_emoji)
    # But discord has too many exceptions for that~
    discord_emoji_name = custom_emojis[custom_emoji]
    full_mutstd_path = os.path.join(extracted_mutstd_path, custom_emoji)
    full_discord_path = os.path.join(discord_emoji_path, discord_emoji_name)
    shutil.copyfile(full_mutstd_path, full_discord_path)
    # print("Added custom {} emoji.".format(discord_emoji_name))
    replace_counter += 1

print("Done, {} emojis replaced.".format(replace_counter))
