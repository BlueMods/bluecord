## DisTok CutTheCord: Better DM Header Patch

This patch replaces the call button on DM headers with search button. The call button is still available in the right menu.

![](https://lasagna.cat/i/9njfeecd.png)

#### Available and tested on:
- 33.1
- 34.0

