## DisTok CutTheCord: Blob Emoji Patch Instructions

This patch is a simple file replacement, so it doesn't have patch files.

You'll just need to pack in the right images to the apk using the `emojireplace.py` script in this folder. See README.md at the root of the repo for information on obtaining the images.

#### Bugs / Side effects
- Not all emojis are replaced
