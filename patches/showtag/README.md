## DisTok CutTheCord: Show Tag Patch

This patch shows user's username after their nickname, if user has a nickname.

Example, no nickname:

![](https://lasagna.cat/i/c28755s6.png)

Example, with nickname:

![](https://lasagna.cat/i/uy68njkr.png)

#### Available and tested on:
- 33.1
- 34.0

